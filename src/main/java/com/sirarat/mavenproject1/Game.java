/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sirarat.mavenproject1;

import java.util.Scanner;

/**
 *
 * @author admin02
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner sc = new Scanner(System.in);
    String inputNewgame;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to ox Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col:");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }

            System.out.println("Error table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void newGame() {
        int rand = ((int) (Math.random() * 100)) % 2;
        if (rand == 0) {
            table = new Table(playerX, playerO);
        } else {
            table = new Table(playerO, playerX);

        }

    }

    public void run() {

        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWinner().getName() + " " + "Win!!");
                }
                System.out.println("Do you want to start a new game?");
                System.out.println("please input Yes or No");
                inputNewgame = sc.next();
                if (inputNewgame.equals("Yes")) {
                    newGame();
                } else {
                    break;
                }

            }
            table.switchPlayer();
        }

    }
}
