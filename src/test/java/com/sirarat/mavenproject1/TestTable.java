/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sirarat.mavenproject1;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author admin02
 */
public class TestTable {

    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

   

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
    @Test
    public void testRow2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
     @Test
    public void testRow3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
    @Test
      public void testCol1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
       @Test
    public void testCol2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
    @Test
    public void testCol3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
    @Test
    public void testdiagonally1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
        @Test
      public void testdiagonally2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
        
    }
      @Test
    public void testRow1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
         table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    @Test
    public void testRow2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
         table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
     @Test
    public void testRow3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
         table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    @Test
      public void testCol1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
       @Test
    public void testCol2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
         table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    @Test
    public void testCol3ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    @Test
    public void testdiagonally1ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
         table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
        @Test
      public void testdiagonally2ByO() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
         table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
      @Test
      public void testXnotwin() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
      
        
    }
      @Test
      public void testXnotwin1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        
        
    }
      @Test
      public void testXnotwin2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        
        
    }
      @Test
      public void testOnotwin() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(false,table.isFinish());
        
        
    }
      @Test
      public void testOnotwin1() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(false,table.isFinish());
        
        
    }
      @Test
      public void testOnotwin2() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(false,table.isFinish());
    }
      
    
    
}
